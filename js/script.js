$(function(){

    const currentTheme = localStorage.getItem("theme");

    let mediaQueryObj = window.matchMedia('(prefers-color-scheme: dark)');
    const isDarkMode = mediaQueryObj.matches;

    if (isDarkMode && currentTheme == "dark"){
        (document.getElementById("theme-toggle")).checked = true;
    }

    if (currentTheme === null){
        if (isDarkMode){
            localStorage.setItem("theme", "dark");
        }else{
            localStorage.setItem("theme", "light");
        }
    }else{
        if (currentTheme == "dark"){
            $("#theme-toggle").prop("checked",true);
        }else{
            $("#theme-toggle").prop("checked",false);
        }
        $("html").attr("data-theme", currentTheme);
    }
    
    $("a.refresh").click(function(e){

        checkChapters();
        e.preventDefault();
        location.reload();
       
    });

    $("#setChapterFilter").click(function(e){

        e.preventDefault();
        checkChapters();
        $("a.refresh").click();
        
    });
    
    $(".qrLink").click(function(e){
        e.preventDefault();
        generateQR(window.location);
    });

    $("body").on("click", ".overlay", function(e){

        $(".overlay").remove();

    });

    $("#theme-toggle").on("change", function(e){

        //true = dark
        let darkMode = this.checked;
        if (darkMode){
            $("html").attr("data-theme", "dark");
            localStorage.setItem("theme", "dark");
        }else{
            $("html").attr("data-theme", "light");
            localStorage.setItem("theme", "light");
        }
    });
    

});

function generateQR(qrText){

    console.log("qrText == "+qrText);

    const newOverlay = document.createElement("div");
    newOverlay.classList.add("overlay");
    newOverlay.setAttribute("id","overlay");

    $("body").append(newOverlay);

    var oQRCode = new QRCode("overlay", {
        text : `${qrText}`,
        width: 600,
        height: 600,
        correctLevel: QRCode.CorrectLevel.H
     });
    
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}


function loadChapters(subjectCode){

    $.getJSON("../../js/Chapters.json", function(list){

        let x = list[subjectCode];

        let url_params = new URLSearchParams(window.location.search);

        $.each(x, function(k, v){

            // let custom = url_params.get("custom");
            var checkedString = "checked = 'checked'";

            let checkedChapter = sessionStorage.getItem("checkedChapter");
            if (checkedChapter != null){

                // let chapter = url_params.get(k);
                if (checkedChapter.includes(k)){
                    checkedString = "checked = 'checked'";
                }else{
                    checkedString = "";
                }

            }


            let tooltipRightStr = "";
            
            //CHECK FOR ORIENTATION FOR MOBILE TOOLTIP
            if (window.matchMedia("(orientation: portrait)").matches){
                tooltipRightStr = "data-placement='right'";
            }   

            let checkbox = "<label for='chapter"+k+"' alt='"+ v +"'>";
            checkbox += "<input type='checkbox' id='chapter"+ k +"' name='"+ k +"' alt='" + v + "' data-tooltip = '" + v + "' "+checkedString+" "+ tooltipRightStr +">";
            checkbox += "Chapter "+k;
            checkbox += "</label>";

            $("#subject_chapters div").append(checkbox);

        });
        
    });

}


function checkChapters(){
    var checkboxes = $("#subject_chapters input[type='checkbox']");
    let checkboxesLen = checkboxes.length;

    var checkedCb = [];

    $(checkboxes).each(function(i){
        if ($(this).prop("checked") == true){
            checkedCb.push($(this).attr("name"));
        }
    });

    sessionStorage.setItem("checkedChapter", JSON.stringify(checkedCb));

}

function getCheckboxNames(){

    var cb = [];

    var checkboxes = $("#subject_chapters input[type='checkbox']");
    $(checkboxes).each(function(i){
        cb.push($(this).attr("name"));
    });

    var cbJSON = JSON.stringify(cb);
    return JSON.parse(cbJSON);

}

function orderQuestionByYear(subjectCode){

    $.getJSON("../../js/"+subjectCode+".json", function (questions) {

        console.log(questions);
        let sortedQuestionMax = questions.sort(function(a,b){return b.question_session-a.question_session})[0];
        let sortedQuestionMin = questions.sort(function(a,b){return a.question_session-b.question_session})[0];
        console.log(sortedQuestionMax);
        console.log(sortedQuestionMin);

    });

}

function loadJson(subjectCode){
    
    $("#question_placeholder > *").addClass("hidden");
    $("#question_placeholder").attr("aria-busy","true").addClass("text-center");

    //TRY ORDER
    //orderQuestionByYear(subjectCode);

    console.log(111);

    $.getJSON("../../js/"+subjectCode+".json", function (questions) {

        let url_params = new URLSearchParams(window.location.search);
        let qid = url_params.get('qid');

        var selection = 0;
        var count = 0;
        var question;
        var r = [];

        if (qid !== null) {

            // count = 1;
            // selection = 1;
            question = questions.find(function (q) {
                return q.question_id == qid
            });

            let sortedQuestionMax = (questions.sort(function(a,b){return b.question_session-a.question_session})[0]).question_session;
            let sortedQuestionMin = (questions.sort(function(a,b){return a.question_session-b.question_session})[0]).question_session;
            let count = questions.length;

            r["count"] = count;
            r["max"] = sortedQuestionMax;
            r["min"] = sortedQuestionMin;
            r["question"] = question;

            $("#question_placeholder div.xOfy").css("display", "none");
            $("#question_placeholder div.id").css("display", "none");
            $("#question_placeholder div.qrDiv").css("display", "block");

        } else {

            let checkedChapter = JSON.parse(sessionStorage.getItem("checkedChapter"));
            let totalChapter = $("#subject_chapters input[type='checkbox']").length;
            
            
            if (checkedChapter === null){
                checkedChapter = getCheckboxNames();
                console.log(checkedChapter);
            }

            let questionBySubject = [];

            if (checkedChapter.length !== totalChapter){

                $("details#subject_chapters").prop("open",true);

                var questionByChapters = [];

                checkedChapter.forEach(function(k){

                    questionByChapters = questions.filter(function (q) {
                        return q.subject_code == subjectCode && q.chapter_number == k
                    });

                    questionBySubject = questionBySubject.concat(questionByChapters);

                });


            }else{

                questionBySubject = questions.filter(function (q) {
                    return q.subject_code == subjectCode
                });
            }


            count = questionBySubject.length;
            selection = getRandomInt(0, count);
            question = questionBySubject[selection];

            let sortedQuestionMax = (questionBySubject.sort(function(a,b){return b.question_session-a.question_session})[0]).question_session;
            let sortedQuestionMin = (questionBySubject.sort(function(a,b){return a.question_session-b.question_session})[0]).question_session;
            // let count = questions.length;
            
            r["count"] = count;
            r["max"] = sortedQuestionMax;
            r["min"] = sortedQuestionMin;
            r["question"] = question;

            $("#question_placeholder div.qrDiv").css("display", "none");

        }

        console.log(r);

        $("h6#sessionRange").html(r.min + "-" + r.max);

        $("#question_placeholder span.questionNumber").html(selection+1);
        $("#question_placeholder span.questionTotal").html(r.count);

        var numbering = r.question.chapter_number +"." + r.question.question_number;

        $("#question_placeholder code.questionNumber").html(numbering);
        $("#question_placeholder p.question_text").html(r.question.question_text);
        $("#question_placeholder .question_metadata .chapter").html("CHAPTER " + r.question.chapter_text);
        $("#question_placeholder .question_metadata .session").html("EXAM " + r.question.question_session);
        $("#question_placeholder .question_metadata .marks").html("<strong>" + r.question.question_marks + "</strong> Mark(s)");
        $("#question_placeholder .question_metadata .id a").prop("href", "index.html?qid=" + r.question.question_id);

        $("#question_placeholder > *").removeClass("hidden");
        $("#question_placeholder").attr("aria-busy","").removeClass("text-center");

        $('html, body').animate({
            scrollTop: $("#question_placeholder").offset().top
        }, 500);

    });


}